class Request {
  constructor(requesterEmail, content) {
    this.requesterEmail = requesterEmail;
    this.content = content;
    this.dateRequested = new Date();
  }
}

class Person {
  constructor() {
    if (this.constructor === Person) {
      throw new Error("Object cannot be created from an abstract class Person");
    }
    if (this.constructor === Employee) {
      if (this.addRequest === undefined) {
        throw new Error("Class must implement addRequest() method");
      }
    }
    if (this.constructor === TeamLead) {
      if (this.addMember === undefined) {
        throw new Error("Class must implement addMember() method");
      }
      if (this.checkRequests === undefined) {
        throw new Error("Class must implement checkRequests() method");
      }
    }
    if (this.constructor === Admin) {
      if (this.addTeamLead === undefined) {
        throw new Error("Class must implement addTeamLead() method");
      }
      if (this.deactivateTeam === undefined) {
        throw new Error("Class must implement deactivateTeam() method");
      }
    }
    if (this.getFullName === undefined) {
      throw new Error("Class must implement getFullName() method");
    }
    if (this.login === undefined) {
      throw new Error("Class must implement login() method");
    }
    if (this.logout === undefined) {
      throw new Error("Class must implement logout() method");
    }
  }
}

class Employee extends Person {
  #firstName;
  #lastName;
  #email;
  #department;
  #isActive;
  #requests;
  constructor(firstName, lastName, email, department) {
    super();
    this.#firstName = firstName;
    this.#lastName = lastName;
    this.#email = email;
    this.#department = department;
    this.#isActive = true;
    this.#requests = [];
  }

  addRequest(content) {
    this.#requests.push(new Request(this.#email, content));
  }

  getFullName() {
    return `${this.#firstName} ${this.#lastName}`;
  }
  login() {
    return `${this.#firstName} ${this.#lastName} has logged in`;
  }
  logout() {
    return `${this.#firstName} ${this.#lastName} has logged out`;
  }

  //   getter
  getFirstName() {
    return this.#firstName;
  }
  getLastName() {
    return this.#lastName;
  }
  getEmail() {
    return this.#email;
  }
  getDepartment() {
    return this.#department;
  }
  getIsActive() {
    return this.#isActive;
  }
  getRequests() {
    return this.#requests;
  }

  //   setter
  setFirstName(firstName) {
    this.#firstName = firstName;
  }
  setLastName(lastName) {
    this.#lastName = lastName;
  }
  setEmail(email) {
    this.#email = email;
  }
  setDepartment(department) {
    this.#department = department;
  }
  setIsActive(isActive) {
    this.#isActive = isActive;
  }
}

class TeamLead extends Person {
  #firstName;
  #lastName;
  #email;
  #department;
  #isActive;
  #members;
  constructor(firstName, lastName, email, department) {
    super();
    this.#firstName = firstName;
    this.#lastName = lastName;
    this.#email = email;
    this.#department = department;
    this.#isActive = true;
    this.#members = [];
  }

  addMember(employee) {
    this.#members.push(employee);
  }

  checkRequests(email) {
    const employee = this.#members.find(
      (employee) => employee.getEmail() == email
    );

    if (employee) {
      return employee.getRequests();
    } else {
      return "No matches are found.";
    }
  }

  getFullName() {
    return `${this.#firstName} ${this.#lastName}`;
  }
  login() {
    return `${this.#firstName} ${this.#lastName} has logged in`;
  }
  logout() {
    return `${this.#firstName} ${this.#lastName} has logged out`;
  }

  //   getter
  getFirstName() {
    return this.#firstName;
  }
  getLastName() {
    return this.#lastName;
  }
  getEmail() {
    return this.#email;
  }
  getDepartment() {
    return this.#department;
  }
  getIsActive() {
    return this.#isActive;
  }
  getMembers() {
    return this.#members;
  }

  //   setter
  setFirstName(firstName) {
    this.#firstName = firstName;
  }
  setLastName(lastName) {
    this.#lastName = lastName;
  }
  setEmail(email) {
    this.#email = email;
  }
  setDepartment(department) {
    this.#department = department;
  }
  setIsActive(isActive) {
    this.#isActive = isActive;
  }
}

class Admin extends Person {
  #firstName;
  #lastName;
  #email;
  #department;
  #teamLeads;
  constructor(firstName, lastName, email, department) {
    super();
    this.#firstName = firstName;
    this.#lastName = lastName;
    this.#email = email;
    this.#department = department;
    this.#teamLeads = [];
  }

  addTeamLead(teamLead) {
    this.#teamLeads.push(teamLead);
  }

  deactivateTeam(email) {
    const teamLead = this.#teamLeads.find(
      (teamLead) => teamLead.getEmail() == email
    );
    if (teamLead) {
      teamLead.setIsActive(false);
      teamLead.getMembers().forEach((member) => {
        member.setIsActive(false);
      });
    }
  }

  getFullName() {
    return `${this.#firstName} ${this.#lastName}`;
  }
  login() {
    return `${this.#firstName} ${this.#lastName} has logged in`;
  }
  logout() {
    return `${this.#firstName} ${this.#lastName} has logged out`;
  }

  //   getter
  getFirstName() {
    return this.#firstName;
  }
  getLastName() {
    return this.#lastName;
  }
  getEmail() {
    return this.#email;
  }
  getDepartment() {
    return this.#department;
  }
  getTeamLeads() {
    return this.#teamLeads;
  }

  //   setter
  setFirstName(firstName) {
    this.#firstName = firstName;
  }
  setLastName(lastName) {
    this.#lastName = lastName;
  }
  setEmail(email) {
    this.#email = email;
  }
  setDepartment(department) {
    this.#department = department;
  }
}

// Creating Employee named John Smith
const employee1 = new Employee();
employee1.setFirstName("John");
employee1.setLastName("Smith");
employee1.setEmail("johnsmith@mail.com");
employee1.setDepartment("IT Department");

// Calling getFullName, login, logout methods from employee
console.log(employee1.getFullName());
console.log(employee1.login());
console.log(employee1.logout());

// Add Request
employee1.addRequest("Requesting for keyboard");
employee1.addRequest("Requesting for mouse");
console.log(employee1.getRequests());

// Creating TeamLead named Jane Doe
const teamLead1 = new TeamLead();
teamLead1.setFirstName("Jane");
teamLead1.setLastName("Doe");
teamLead1.setEmail("janedoe@mail.com");
teamLead1.setDepartment("IT Department");

// Calling getFullName, login, logout methods from teamLead
console.log(teamLead1.getFullName());
console.log(teamLead1.login());
console.log(teamLead1.logout());

// Add employee1 to member
teamLead1.addMember(employee1);
console.log(teamLead1.getMembers());
console.log(teamLead1.checkRequests("johnsmith@mail.com"));
// checking requests for email that is not existing
console.log(teamLead1.checkRequests("notExistingEmail@mail.com"));

// Adding admin named Admin User
const admin1 = new Admin();
admin1.setFirstName("Admin");
admin1.setLastName("User");
admin1.setEmail("adminuser@mail.com");
admin1.setDepartment("IT Department");

// Calling getFullName, login, logout methods from admin
console.log(admin1.getFullName());
console.log(admin1.login());
console.log(admin1.logout());

// Add existing teamLead to teamLeads Array
admin1.addTeamLead(teamLead1);
console.log(admin1.getTeamLeads());

// sets the isActive property of the team lead and all the team members to false
admin1.deactivateTeam("janedoe@mail.com");
console.log(teamLead1);
console.log(teamLead1.getMembers());
