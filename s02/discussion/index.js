/* 
    Polymorphism - Allows object to take on different forms

    Involves the concept of overriding, where a method in subclass (derived class) overrides the implementation of a method with the same name in its superclass (base class)

 */

class Person {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  getFullName() {
    return `The person's name is ${this.firstName} ${this.lastName}`;
  }
}

class Employee extends Person {
  constructor(employeeId, firstName, lastName) {
    super(firstName, lastName);
    this.employeeId = employeeId;
  }

  //   Overriding
  getFullName() {
    return super.getFullName() + ` with employeeId ${this.employeeId}`;
  }
}

const employeeA = new Employee("EM-004", "John", "Smith");
console.log(employeeA.getFullName());

class TeamLead extends Employee {
  getFullName() {
    return super.getFullName() + " and he/she is a team leader";
  }
}

const teamLead = new TeamLead("TL-001", "Jane", "Smith");
console.log(teamLead.getFullName());

class Animal {
  speak() {
    console.log("Default Speak");
  }
}

class Dog extends Animal {}

const dog = new Dog();
