/* 
    Encapsulation refers to the grouping of data, along with the methods that operate on those data, into a single unit

    Abstraction and encapsulation are complementary concepts: abstraction focuses on the observable behavior of an object...encapsulation focuses on the implementation that gives rise to this behavior

    Control access of data in encapsulation is with the use of accessor methods in the form of getters and setters
*/

// This class will serve as blue print for Employee Class
class Person {
  // Rules on what to create
  constructor() {
    if (this.constructor === Person) {
      throw new Error("Object cannot be created from an abstract class Person");
    }
    if (this.getFullName === undefined) {
      throw new Error("Class must implement getFullName() method");
    }
  }
}

class Employee extends Person {
  // Encapsulation aided by private fields (#), ensures data protection. Setters and getters provide
  // private fields
  #firstName;
  #lastName;
  #employeeID;
  constructor(firstName, lastName, employeeID) {
    super();
    this.#firstName = firstName;
    this.#lastName = lastName;
    this.#employeeID = employeeID;
  }

  // getters methods
  getFirstName() {
    return `First Name: ${this.#firstName}`;
  }
  getLastName() {
    return `Last Name: ${this.#lastName}`;
  }
  getEmployeeId() {
    return this.#employeeID;
  }
  getFullName() {
    return `${this.#firstName} ${this.#lastName} has employee ID of ${
      this.#employeeID
    }`;
  }

  //   setters methods
  setFirstName(firstName) {
    this.#firstName = firstName;
  }
  setLastName(lastName) {
    this.#lastName = lastName;
  }
  setEmployeeID(employeeID) {
    this.#employeeID = employeeID;
  }
}

const employeeA = new Employee("John", "Smith", "EM-001");
// Direct access with the field / property firstName will return undefined because the property is private
console.log(employeeA.firstName);
console.log(employeeA.getFirstName());

// We could directly change the value of the property firstName because the property is private
employeeA.setFirstName("David");
console.log(employeeA.getFirstName());
console.log(employeeA.getFullName());

const employeeB = new Employee();
console.log(employeeB.getFullName());
employeeB.setFirstName("Jill");
employeeB.setLastName("Hill");
employeeB.setEmployeeID("EM-002");
console.log(employeeB.getFullName());
